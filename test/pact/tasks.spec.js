const assert = require('assert')
const { Pact, Matchers } = require('@pact-foundation/pact')
import { service } from '../../src/components/api.js'
const { eachLike } = Matchers

describe('Pact with to-do API', () => {
  const provider = new Pact({
    port: 4444,
    consumer: 'Client',
    provider: 'Api',
  })

  beforeAll(() => provider.setup())

  afterAll(() => provider.finalize())

  describe('when a call to the API is made', () => {
    beforeAll(async () => {
      return provider.addInteraction({
        state: 'there are tasks',
        uponReceiving: 'a request for tasks',
        withRequest: {
          path: '/getTask',
          method: 'GET',
        },
        willRespondWith: {
          body: eachLike({
              id: 0,
              title: "Task 1",
          }),
          status: 200,
          headers: {
            "Access-Control-Allow-Origin": "*",
            'Content-Type': 'application/json; charset=utf-8'
        },

        },
      })
    })

    it('will receive the list of current tasks', async () => {
        var a = []
        const result =  await service.get("http://localhost:4444/getTask")
            .then((response) => {
                a = response.data
            });
        assert.ok(a.length)
      
  
    })
  })
})
