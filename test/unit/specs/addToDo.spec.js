import Vue from 'vue'
import addToDo from '@/components/addToDo'


describe('addToDo.vue', () => {

  let addComp = null;
    beforeEach(() => {
      const container = document.createElement('div');
      const addToDoComponent = Vue.extend(addToDo);
      addComp = new addToDoComponent();
      addComp.$mount(container);
    });

    it('should render correct text', () => {
      expect(addComp.$el.querySelector('.addMain .strongText').textContent).toBe('TO DO LIST')
    })
    
    it('should render input text box', () => {
      const input = addComp.$el.querySelector('.addMain .inputText')
      expect(input.tagName).toBe('INPUT')
    })

    it('should get correct text from input text box', () => {
      const input = addComp.$el.querySelector('.addMain .inputText')
      input.value = 'Task 1'
      input.dispatchEvent(new Event('input'))
      expect(addComp.inputText).toBe('Task 1')
    })

    it('should render add button', () => {
      const button = addComp.$el.querySelector('.addMain .addButton')
      expect(button.tagName).toBe('BUTTON')
    })
  })
