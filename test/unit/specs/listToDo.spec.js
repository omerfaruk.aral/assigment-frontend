import Vue from 'vue'
import listToDo from '@/components/listToDo'
import addToDo from '@/components/addToDo'

describe('listToDo.vue', () => {

    let listComp = null;
    let addComp = null;
    beforeEach(() => {
        const container = document.createElement('div');
        const listToDoComponent = Vue.extend(listToDo);

        listComp = new listToDoComponent({
            propsData: {
                toDo: []
            }
        });
        listComp.$mount(container);

        const container1 = document.createElement('div');
        const addToDoComponent = Vue.extend(addToDo);
        addComp = new addToDoComponent({});
        addComp.$mount(container1);
    });
    
    it('should lists tasks from todo list', () => {
        expect(listComp.$el.querySelectorAll('li').length).toBe(0);
    })

    it('should render correct text', () => {
        expect(listComp.$el.querySelector('.listMain .strongText').textContent).toBe('Your To-dos')
    })

    it('should render correct contents ', () => {
        const input = listComp.$el.querySelector('.listMain .strongText')
        expect(input.tagName).toBe('STRONG')
    })  
})
 