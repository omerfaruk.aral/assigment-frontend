context('Actions', () => {
    it('successfully loads', () => {
        cy.visit('http://localhost:8081') // change URL to match your dev URL
    })

    it('add new task', () => {
        cy.visit('http://localhost:8081')
        cy.get('input.inputText').type('for button', { delay: 100 })
        cy.get('.addButton').click()
        cy.wait(350)
        cy.get('input.inputText').type('for key', { delay: 100 })
        cy.get('input.inputText').type('{enter}')
        cy.wait(350)
        cy.get('.taskList').first().should('contain', 'for button')
        cy.get('.taskList').last().should('contain', 'for key')
        cy.wait(5000)
    })

})