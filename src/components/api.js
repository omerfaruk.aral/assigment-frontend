import axios from 'axios'

export const service = axios.create({
    timeout: 10000,
    headers: {
        common: {
            Accept: "application/json",
        },
        'Content-Type': "application/json; charset=UTF-8",
    }
})
