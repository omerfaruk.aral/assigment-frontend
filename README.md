<h1 align="center">Welcome to assigment-frontend 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D3.0.0-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D6.0.0-blue.svg" />
  <a href="https://github.com/kefranabg/readme-md-generator#readme" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://github.com/kefranabg/readme-md-generator/graphs/commit-activity" target="_blank">
    <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" />
  </a>

</p>

> CLI that generates beautiful README.md files.
## To-do App Assigment
 - This is a simple to-do app. You can add your todos
 - Backend side was coded with Golang
 - Frontend side was coded with Vue Js

## Links
 - [Frontend](https://assigment-frontend-5d4wm.ondigitalocean.app/)
 - [Backend](https://assigment-backend-k3r9w.ondigitalocean.app/)  

## Components
 - addToDo
 - listToDo
## Test
 - This project was developed using A-TDD approach.

### Consumer Test
 - I used pact tool for consumer test.

### Acceptance Test
 - I used cypress for acceptance tests. This framework opens a different application when it runs, so it could not be added to the pipeline

### CI/CD
 - I used Gitlab pipeline for this project.


## Prerequisites

  - node >= 6.0.0
  - npm  >= 3.0.0
## Install

```sh
npm install
```

## Usage

```sh
npm run dev
```

## Run tests

```sh
npm run test
```
## Run acceptance tests

```sh
npx cypress open
```
## Run consumer test

```sh
npm run test:pact
```
## Author

👤 **Ömer Faruk Aral**

* Gitlab: [@omerfaruk.aral](https://gitlab.com/omerfaruk.aral)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/kefranabg/readme-md-generator/issues). You can also take a look at the [contributing guide](https://github.com/kefranabg/readme-md-generator/blob/master/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2021 [Ömer Faruk Aral](https://gitlab.com/omerfaruk.aral).<br />
This project is [MIT](https://github.com/kefranabg/readme-md-generator/blob/master/LICENSE) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
